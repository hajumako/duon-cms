<?php
/**
 * Created by PhpStorm.
 * User: Konrado
 * Date: 2016-07-28
 * Time: 14:59
 */

return [
    'MAIL_HOST' => env('MAIL_HOST', 'localhost'),
    'MAIL_ENCRYPTION' => env('MAIL_ENCRYPTION', 'tls'),
    'MAIL_PORT' => env('MAIL_PORT', '465'),
    'MAIL_USERNAME' => env('MAIL_USERNAME', ''),
    'MAIL_PASSWORD' => env('MAIL_PASSWORD', ''),
    'MAIL_FROM' => env('MAIL_FROM', ''),
    'MAIL_REPLY' => env('MAIL_REPLY', '')
];
var seasonDefaultTxt = "----",
    usageDefaultTxt = "---- kWh",
    usageBDefaultTxt = "---- kWh/rok",
    savingsDefaultTxt = "---,-- zł",
    c = new Constants();

$(function() {

    $('input.datepicker').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent: false
    });

    $vendorWrapper = $('#wrapper-vendor');
    $eVendorWrapper = $('#wrapper-vendor-e');
    $gVendorWrapper = $('#wrapper-vendor-g');
    $eVendor = $('select[name="vendor-e"]');

    $usage = $('.usage');
    $savings = $('.savings');
    
    $eWrapper = $('#wrapper-e');
    $gWrapper = $('#wrapper-g');

    $led = $('input[name="led"]');
    $led.val(c.getBulbs($('input[name="product"]:checked').val()));

    $name = $('input[name="name"]');
    $surname = $('input[name="surname"]');
    $phone = $('input[name="phone"]');
    $email = $('input[name="email"]');
    
    $userInputs = $('.userInput');
    $eUserInputs = $('.userInput', $eWrapper);
    $gUserInputs = $('.userInput', $gWrapper);

    $sSavings = $('#savings-s');

    $('input[name="product"]').change(function () {
        $led.val(c.getBulbs($(this).val()));
        toggleFields();
        resetFields();
        bValidate();
        updateSummary()
    });

    $('input[name="calc-type"]').change(function () {
        toggleFields();
        resetFields();
        bValidate();
        updateSummary()
    });

    $modal = $('#response-modal-wrapper');
    $page = $('#page-wrapper');

    function toggleFields() {
        $('.info').removeClass('show');

        var prodVal = $('input[name="product"]:checked').val(),
            calcVal = $('input[name="calc-type"]:checked').val();

        $userInputs.removeClass('error');
        $eUserInputs.removeClass('required');
        $gUserInputs.removeClass('required');

        if(prodVal == 1) {
            $vendorWrapper.removeClass('trans').addClass('collapsed');
            $gVendorWrapper.addClass('hide2');
            $eWrapper.removeClass('hide');
            $gWrapper.addClass('hide');
            toggleInputs(calcVal, $eWrapper);
        } else if(prodVal == 2) {
            $vendorWrapper.addClass('trans').removeClass('collapsed');
            $gVendorWrapper.removeClass('hide2');
            $eWrapper.addClass('hide');
            $gWrapper.removeClass('hide');
            toggleInputs(calcVal, $gWrapper);
        } else if(prodVal == 3) {
            $vendorWrapper.removeClass('trans').removeClass('collapsed');
            $gVendorWrapper.removeClass('hide2');
            $eWrapper.removeClass('hide');
            $gWrapper.removeClass('hide');
            toggleInputs(calcVal, $eWrapper);
            toggleInputs(calcVal, $gWrapper);
        }

        if(calcVal == 1) {
            $('.invoice').removeClass('hide').addClass('required');
            $('.no-invoice').addClass('hide').removeClass('required');
        } else if(calcVal == 2) {
            $('.invoice').addClass('hide').removeClass('required');
            $('.no-invoice').removeClass('hide').addClass('required');
        }
    }

    function toggleInputs(calcVal, wrapper) {
        if(calcVal == 1)
            $('.invoice .userInput', wrapper).addClass('required');
        else if(calcVal == 2)
            $('.no-invoice .userInput', wrapper).addClass('required');
    }

    function resetFields() {
        $usage.val(usageDefaultTxt);
        $savings.val(savingsDefaultTxt);
        $savings.attr('data-val', 0);
    }

    toggleFields();

    $('.toggleBtn').click(function() {
        $(this).parent().toggleClass('opened');
    });

    $('#step1').click(function() {
        $.scrollTo($('#form-fortum'), 600, { easing:'inout' });
    });
    $('#step2').click(function() {
        $.scrollTo($('#wrapper-e'), 600, { easing:'inout' });
    });
    $('#step3').click(function() {
        $.scrollTo($('#wrapper-contact-data'), 600, { easing:'inout' });
    });

    $.easing.inout = function (t) {
        return t < .5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;
    };

    //summary

    function updateSummary() {
        var e = Number($eSavings.attr('data-val')) || 0,
            g = Number($gSavings.attr('data-val')) || 0,
            b = Number($bSavings.attr('data-val')) || 0,
            s = e + g + b;
        if(s == 0)
            $sSavings.val(savingsDefaultTxt);
        else
            $sSavings.val(s.toFixed(2).replace('.', ',') + " zł");
    }

    //energy

    $eTariff = $('select[name="tariff-e"]');
    $eDateF = $('input[name="date-from-e"]');
    $eDateT = $('input[name="date-to-e"]');
    $eQuantity = $('input[name="quantity-e"]');
    $eCost = $('input[name="cost-e"]');

    $eUsage = $('input[name="usage-e"]');
    $eSavings = $('input[name="savings-e"]');

    $eDateF.on("dp.change", function (e) {
        $eDateT.data("DateTimePicker").minDate(e.date);
        eValidate();
    });
    $eDateT.on("dp.change", function (e) {
        $eDateF.data("DateTimePicker").maxDate(e.date);
        eValidate();
    });
    $eUserInputs.on('input select', eValidate);
    $('.userInput', $eVendorWrapper).on('input select', eValidate);

    function eValidate() {
        var vendor = $eVendor.val(),
            calcVal = $('input[name="calc-type"]:checked').val(),
            dateF = moment($eDateF.val(), 'DD/MM/YYYY'),
            dateT = moment($eDateT.val(), 'DD/MM/YYYY'),
            quantity = Number($eQuantity.val().replace(',', '.')),
            tariff = $eTariff.val(),
            cost = Number($eCost.val().replace(',', '.')),
            savings, usage;
        if(calcVal == 1 && dateF.isValid() && dateT.isValid() && dateT.diff(dateF) >= 0 && quantity > 0 && tariff > 0 && vendor != 0) {
            usage = eCountUsageFV(dateF, dateT, quantity);
            $eUsage.val(Math.round(usage) + " kWh");
            savings = eCountSavingsFV(usage, vendor);
            $eSavings.val(savings.toFixed(2).replace('.', ',') + " zł");
            $eSavings.attr('data-val', savings);
        } else if(calcVal == 2 && cost > 0 && vendor != 0) {
            usage = eCountUsageNFV(cost, vendor);
            $eUsage.val(Math.round(usage) + " kWh");
            savings = eCountSavingsNFV(usage, vendor);
            $eSavings.val(savings.toFixed(2).replace('.', ',') + " zł");
            $eSavings.attr('data-val', savings);
        } else {
            $eUsage.val(usageDefaultTxt);
            $eSavings.val(savingsDefaultTxt);
            $eSavings.attr('data-val', 0);
        }
        updateSummary();
    }

    function eCountUsageFV(dateF, dateT, quantity) {
        var dateDiff = dateT.diff(dateF, 'days');
        return quantity / (dateDiff + 1) * 365;
    }

    function eCountSavingsFV(usage, vendor) {
        if($('input[name="product"]:checked').val() == "2")
            return 0;
        else {
            var freeEnergy = (usage / 12 * 0.6).toFixed(2);
            return freeEnergy * c.getEnergyPrice(vendor) * 1.23 + (c.getEnergyPrice(vendor) - c.getEnergyPrice("DUON")) * 2 * usage * 1.23 + (c.getOH(vendor) * 24 * 1.23);
        }
    }

    function eCountUsageNFV(cost, vendor) {
        return (cost / 1.23 - c.getEnergyNetworkConstant(vendor) - c.getOH(vendor)) / (c.getEnergyPrice(vendor) + c.getEnergyNetworkVariable(vendor)) * 12;
    }

    function eCountSavingsNFV(usage, vendor) {
        if($('input[name="product"]:checked').val() == "2")
            return 0;
        else {
            var freeEnergy = (usage / 12 * 0.6).toFixed(2);
            return freeEnergy * c.getEnergyPrice(vendor) * 1.23 + (c.getEnergyPrice(vendor) - c.getEnergyPrice("DUON")) * 2 * usage * 1.23 + (c.getOH(vendor) * 24 * 1.23);
        }
    }

    //gas

    $gTariff = $('select[name="tariff-g"]');
    $gDateF = $('input[name="date-from-g"]');
    $gDateT = $('input[name="date-to-g"]');
    $gQuantity = $('input[name="quantity-g"]');
    $gCost = $('input[name="cost-g"]');

    $gSeason = $('input[name="season-g"]');
    $gUsage = $('input[name="usage-g"]');
    $gSavings = $('input[name="savings-g"]');

    $gDateF.on("dp.change", function (e) {
        $gDateT.data("DateTimePicker").minDate(e.date);
        gValidate();
    });
    $gDateT.on("dp.change", function (e) {
        $gDateF.data("DateTimePicker").maxDate(e.date);
        gValidate();
    });
    $gUserInputs.on('input select', gValidate);

    function gValidate() {
        var calcVal = $('input[name="calc-type"]:checked').val(),
            dateF = moment($gDateF.val(), 'DD/MM/YYYY'),
            dateT = moment($gDateT.val(), 'DD/MM/YYYY'),
            quantity = Number($gQuantity.val().replace(',', '.')),
            tariff = $gTariff.val(),
            cost = Number($gCost.val().replace(',', '.')),
            savings, usage;
        if(calcVal == 1 && dateF.isValid() && dateT.isValid() && dateT.diff(dateF) >= 0 && quantity > 0 && tariff > 0) {
            usage = gCountUsageFV(dateF, dateT, quantity, tariff);
            $gUsage.val(Math.round(usage) + " kWh");
            savings = gCountSavingsFV(usage);
            $gSavings.val(savings.toFixed(2).replace('.', ',') + " zł");
            $gSavings.attr('data-val', savings);
        } else if(calcVal == 2 && cost > 0) {
            usage = gCountUsageNFV(cost);
            $gUsage.val(Math.round(usage) + " kWh");
            savings = gCountSavingsNFV(usage);
            $gSavings.val(savings.toFixed(2).replace('.', ',') + " zł");
            $gSavings.attr('data-val', savings);
        } else {
            $gSeason.val(seasonDefaultTxt);
            $gUsage.val(usageDefaultTxt);
            $gSavings.val(savingsDefaultTxt);
            $gSavings.attr('data-val', 0);
        }
        updateSummary();
    }

    function gCountUsageNFV(cost) {
        if(cost < 50)
            return 12 * (cost / 1.23 - c.getGasSubscriptionPrice("W1") - c.getGasNetworkConstant("W1")) / (c.getGasPrice("PGNIG", "W1") + c.getGasNetworkVariable("W1"));
        else
            return (cost / 1.23 - c.getGasSubscriptionPrice("W2") - c.getGasNetworkConstant("W2")) / (c.getGasPrice("PGNIG", "W2") + c.getGasNetworkVariable("W2")) / 0.22;
    }

    function gCountSavingsNFV(usage) {
        if($('input[name="product"]:checked').val() == "1")
            return 0;
        else
            return (c.getGasPrice("PGNIG", "W1") - c.getGasPrice("DUON", "r2016")) * usage * 1.23;
    }

    function gCountUsageFV(dateF, dateT, quantity, tariff) {
        dateDiff = dateT.diff(dateF, 'days');
        if(tariff == 1) {
            return quantity / (dateDiff + 1) * 365;
        } else {
            var season = c.getSeason(dateF);
            var seasonInd = c.getSeasonIndicator(season);
            return quantity / (dateDiff + 1) * 91.2 / seasonInd;
        }
    }
    
    function gCountSavingsFV(usage) {
        if($('input[name="product"]:checked').val() == "1")
            return 0;
        else
            return (c.getGasPrice("PGNIG", "W1") - c.getGasPrice("DUON", "r2016")) * usage * 1.23;
    }

    //bulbs
    $bTime = $('input[name="usage-time-b"]');
    $bUsageStd = $('input[name="usage-std-b"]');
    $bUsageLed = $('input[name="usage-led-b"]');
    $bSavingsEnergy = $('input[name="savings-e-b"]');
    $bSavings = $('input[name="savings-b"]');

    $bTime.on('input', bValidate);
    $('.userInput', $eVendorWrapper).on('input select', bValidate);

    function bValidate() {
        var bulbs = $led.val(),
            time = Number($bTime.val()),
            vendor = $eVendor.val(),
            savings;
        if(bulbs > 0 && time > 0 && vendor != 0) {
            var usageStd = countBulbsStd(bulbs, time);
            $bUsageStd.val(Math.round(usageStd) + " kWh");
            var usageLed = countBulbsLed(bulbs, time);
            $bUsageLed.val(Math.round(usageLed) + " kWh");
            var savingsEnergy = usageStd - usageLed;
            $bSavingsEnergy.val(Math.round(savingsEnergy) + " kWh");
            if($('input[name="calc-type"]:checked').val() == 1)
                savings = bCountSavingsFV(savingsEnergy, vendor);
            else
                savings = bCountSavingsNFV(savingsEnergy, vendor);
            $bSavings.val(savings.toFixed(2).replace('.', ',') + " zł");
            $bSavings.attr('data-val', savings);
        } else {
            $bUsageStd.val(usageBDefaultTxt);
            $bUsageLed.val(usageBDefaultTxt);
            $bSavingsEnergy.val(usageBDefaultTxt);
            $bSavings.val(savingsDefaultTxt);
            $bSavings.attr('data-val', 0);
        }
        updateSummary();
    }

    function countBulbsStd(bulbs, time) {
        if(bulbs == 4)
            return (4 * c.getPower("std", 1) * (time / 60) * 365 ) / 1000;
        else
            return (2 * c.getPower("std", 1) * (time / 60) * 365 ) / 1000;
    }

    function countBulbsLed(bulbs, time) {
        if(bulbs == 4)
            return (4 * c.getPower("led", 1) * (time / 60) * 365 ) / 1000;
        else
            return (2 * c.getPower("led", 1) * (time / 60) * 365 ) / 1000;
    }

    function bCountSavingsFV(savingsEnergy, vendor) {
        return savingsEnergy * 2 * c.getBulbPrice(vendor) * 1.23;
    }

    function bCountSavingsNFV(savingsEnergy, vendor) {
        return savingsEnergy * 2 * c.getBulbPrice(vendor) * 1.23;
    }
    
    
    //validateForm
    $userInputs.on('focus, click', function () {
        $(this).removeClass("error");
        $('.info').removeClass('show');
    });

    $phone.on('focus, click', function () {
        $('#error-phone').removeClass('show');
    });

    sending = false;
    $('#form-fortum').submit(function(event) {
        event.preventDefault();
        if(!sending) {
            $userInputs.removeClass("error");
            $('.info').removeClass('show');
            var error = false,
                p_error = false;

            $.each($('.userInput.required'), function (i, element) {
                if (element.value == 0 || element.value == "0" || element.value == "" || element.value == null) {
                    $(element).addClass("error");
                    error = true;
                } else if (element.type && element.type === 'checkbox' && !element.checked) {
                    $(element).addClass("error");
                    error = true;
                }
                if(element.id == "phone") {
                    phone = element.value.replace(/[^0-9]/g, '');
                    if (phone.length != 9) {
                        $(element).addClass("error");
                        p_error = true;
                    }
                }
            });
            if (!error && !p_error) {
                $('.uil-fortum-css').addClass('show');
                sending = true;
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });

                jQuery.post(
                    'calculate',
                    jQuery(this).serialize(),
                    'json'
                )
                    .done(function (data) {
                        $('.uil-fortum-css').removeClass('show');
                        showModal(200);
                        sending = false;
                    })
                    .error(function (data) {
                        $('.uil-fortum-css').removeClass('show');
                        showModal(500);
                        sending = false;
                    });
            
            } else if(error) {
                $('#error').addClass('show');
            } else if(p_error) {
                $('#error-phone').addClass('show');
            }
        }
        return false;
    });

    function showModal(status) {
        st = $(window).scrollTop() - 100;
        $page.css('top', '-' + st + 'px').addClass('blur');
        $modal.removeClass().addClass('status' + status).fadeIn(300);
    }

    $('#close-modal').click(function () {
        $page.removeClass('blur');
        $(window).scrollTop(st + 100);
        $modal.fadeOut(300);
        if($modal.hasClass('status200')) {
            resetFields();
            $('input[type="text"].userInput').val('');
            $('select.userInput').val('0');
            $('select[name="vendor-g"]').val('PGNIG');
        }
    });

    function preFillForm() {
        $('input[name="name"]').val('aa').change();
        $('input[name="surname"]').val('bb').change();
        $('input[name="phone"]').val('123456789').change();
        $('input[name="email"]').val('k.c.kucharski@gmail.com').change();

        //$('input[name="calc-type"]').val('2').change();
        $('select[name="vendor-e"]').val("PGE").change();
        $('input[name="cost-g"]').val('123').change();
        $('input[name="cost-e"]').val('123').change();
        $('input[name="usage-time-b"]').val('12').change();

        eValidate();
        gValidate();
        bValidate();
    }

    //todo remove on publish
    //preFillForm();
});
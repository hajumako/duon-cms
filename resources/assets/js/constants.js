/**
 * Created by Konrado on 2016-07-20.
 */

function Constants() {

    /* Miesięc */
    var monthsIndicator = [
        0.154929577,
        0.133802817,
        0.105633803,
        0.070422535,
        0.035211268,
        0.035211268,
        0.035211268,
        0.035211268,
        0.070422535,
        0.091549296,
        0.105633803,
        0.126760563
    ];

    /* Miesiąc	Pora roku */
    var seasons = [
        'zima',
        'zima',
        'wiosna',
        'wiosna',
        'wiosna',
        'lato',
        'lato',
        'lato',
        'jesień',
        'jesień',
        'jesień',
        'zima'
    ];

    /* GAZ */
    var gasPrice = {
        PGNIG: {
            W1: 0.0983,
            W2: 0.0983,
            W3: 0.0983,
            W4: 0
        },
        DUON: {
            r2016: 0.0885,
            r2017: 0.0885,
            r2018: 0.0983,
            r2019: 0.0983
        }
    };

    /* Abonament */
    var gasSubscriptionPrice = {
        W1: 3.76,
        W2: 6.28,
        W3: 6.28,
        W4: 0
    };

    /* Stawka sieciowa stała */
    var gasNetworkConstant = {
        W1: 4.085,
        W2: 23.01,
        W3: 23.01,
        W4: 0
    };

    /* Stawka sieciowa zmienna */
    var gasNetworkVariable = {
        W1: 0.05413,
        W2: 0.03917,
        W3: 0.03917,
        W4: 0
    };

    /* EE czynna */
    var energyPrice = {
        PGE:	0.253,
        TAURON:	0.2546,
        ENERGA:	0.2521,
        ENEA:	0.2545,
        RWE:	0.2762,
        DUON:	0.2495
    };

    /* Sieciowa stała+Abonament+Przejściowa */
    var energyNetworkConstant = {
        PGE:    8.66,
        TAURON: 7.86,
        ENERGA: 9.02,
        ENEA:   10.06,
        RWE:    13.05,
        DUON:   0

    };

    /* Sieciowa zmienna+jakościowa */
    var energyNetworkVariable = {
        PGE:    0.2162,
        TAURON: 0.1751,
        ENERGA: 0.2425,
        ENEA:   0.1750,
        RWE:    0.1515,
        DUON:   0
    };
    
    /* OH */
    var OH = {
        PGE:    0,
        TAURON: 0,
        ENERGA: 0,
        ENEA:   0,
        RWE:    4.29,
        DUON:   0
    };

    var bulbs = [
        null,
        2,
        2,
        4
    ];
    
    var power = {
        std: [
            45,
            60
        ],
        led: [
            6,
            10
        ]
    };

    this.getMonthsIndicator = function (i) {
        return monthsIndicator[i];
    };

    this.getSeason = function (date) {
        return seasons[date.month()];
    };

    this.getSeasonIndicator = function (season) {
        return seasons.reduce(function (a, e, i) {
            if (e === season)
                a += monthsIndicator[i];
            return Number(a);
        }, []);
    };
    
    this.getGasPrice = function (vendor, tariff) {
        return gasPrice[vendor][tariff];
    };

    this.getGasSubscriptionPrice = function (tariff) {
        return gasSubscriptionPrice[tariff];
    };   
    
    this.getGasNetworkConstant = function (tariff) {
        return gasNetworkConstant[tariff];
    };

    this.getGasNetworkVariable = function (tariff) {
        return gasNetworkVariable[tariff];
    };
    
    this.getEnergyPrice = function (vendor) {
        return energyPrice[vendor];
    };

    this.getEnergyNetworkConstant = function (vendor) {
        return energyNetworkConstant[vendor];
    };
    
    this.getOH = function (vendor) {
        return OH[vendor];
    }
    
    this.getEnergyNetworkVariable = function (vendor) {
        return energyNetworkVariable[vendor];
    }
    
    this.getBulbs = function (product) {
        return bulbs[product];
    }
    
    this.getPower = function (type, i) {
        return power[type][i];
    }

    this.getBulbPrice = function (vendor) {
        return energyPrice[vendor] + energyNetworkVariable[vendor];
    }
}
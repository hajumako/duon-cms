<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Fortum | Admin</title>

    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#fff">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"
          integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link href="{{ url('css/admin.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ url('js/jquery-2.1.1.min.js') }}"></script>

</head>
<body id="app-layout">
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ url('img/logo-fortum_c.png') }}">
            </a>
            @if (!Auth::guest())
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="{{ url('/zgloszenia') }}">
                            Zgłoszenia
                        </a>
                        <a href="{{ url('/parametry') }}">
                            Parametry
                        </a>
                    </li>
                </ul>
            @endif
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                @if (!Auth::guest())
                    <li>
                        <a class="excel" href="{{ url('/zgloszenia/generuj') }}">
                            <span>e</span> Generuj
                        </a>
                            <span class="username">
                                {{ Auth::user()->name }}
                            </span>

                        <a href="{{ url('/logout') }}">
                            <i class="fa fa-btn fa-sign-out"></i>Wyloguj
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>

<div class="wrapper">
    @yield('content')
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
        crossorigin="anonymous"></script>
</body>
</html>

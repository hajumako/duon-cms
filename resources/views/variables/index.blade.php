@extends('layouts.master_admin')

@section('content')
    <h1>Parametry</h1>

    @foreach($variables as $i => $variable)
        <div class="variable-wrapper">
            <h3>{{ $variable->title }}</h3>
            <h6>Komórki w Excelu: {{ $variable->cells_in_excel }}</h6>
            <a href="{{ url('parametry/' . $variable->id . '/edit') }}">Edytuj</a>
            <table>
                <tbody>
                @foreach($variable->data as $j => $data)
                    <tr>
                        <td class="name">
                            {{ $j }}
                        </td>
                        <td class="value">
                            {{ $data }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endforeach

@endsection
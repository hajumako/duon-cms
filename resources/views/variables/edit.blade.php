@extends('layouts.master_admin')

@section('content')

    {!! Form::model($variable, ['method' => 'PATCH', 'action' => ['VariablesController@update', $variable->id]]) !!}

    <div class="variable-wrapper">
        <h3>{{ $variable->title }}</h3>
        <h6>Komórki w Excelu: {{ $variable->cells_in_excel }}</h6>
        {!! Form::submit('Zapisz', null) !!}
        <a href="{{ url('/parametry') }}">Anuluj</a>

        <table class="no-border">
            <tbody>
            @foreach($variable->data as $i => $data)
                <tr>
                    <td class="name">
                        {{ $i }}
                    </td>
                    <td class="value">
                        <input id="data-{{ $i }}" type="text" name="data[{{ $i }}]" value="{{ $data }}"/>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if($variable->value_is_text)
            <div class="error-msg">Pola nie mogą być puste</div>
        @else
            <div class="error-msg">Pola nie mogą być puste oraz muszą być liczbami</div>
        @endif
    </div>

    {!! Form::close() !!}

    <script>
        var value_is_text = {!! $variable->value_is_text !!};
        $('form').submit(function (e) {
            var err = false,
                $inputs = $('input[type="text"]');
            $('.error-msg').removeClass('show');
            $inputs.removeClass("error");
            $.each($inputs, function (i, el) {
                if (el.value == '' || (!$.isNumeric(el.value) || value_is_text)) {
                    err = true;
                    $(el).addClass("error");
                }
            });
            if (err) {
                $('.error-msg').addClass('show');
                e.preventDefault();
                return false;
            }
        });
    </script>

@endsection
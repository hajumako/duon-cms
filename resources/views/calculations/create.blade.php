@extends('layouts.master_front')

@section('content')

    <div id="page-wrapper">
        <header class="header wrapper">
            <div id="b1"></div>
            <div id="b2"></div>
            <img class="logo" src="{{ url('img/logo-fortum.png') }}" alt="Fortum"/>
            <div class="wrap">
                <h1>Kalkulator<br>Oszczędności</h1>
            </div>
            <div class="intro">
                <div id="b3"></div>
                <div id="b4"></div>
                <h2>
                    Sprawdź, ile możesz zaoszczędzić na gazie i prądzie przechodząc do <span>FORTUM</span> oraz dowiedz się, czemu żarówki LED są lepsze niż zwykłe.
                </h2>
            </div>
            <div id="step1" class="start">
                <h3>Rozpocznij Analizę</h3>
                <div class="start-btn2"></div>
            </div>
        </header>

        {!! Form::open(array('id' => 'form-fortum', 'class' => 'wrapper', 'name' => 'form-fortum', 'action' => 'CalculationsController@store')) !!}

        @if($errors->any())
            <ul id="error-list">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        <div id="wrapper-calc-type">
            <div>
                <label>
                    <div class="icon fv"></div>
                    {!! Form::radio('calc-type', '1', true) !!}
                    Analiza z wykorzystaniem faktury
                    <div class="radio">
                        <div></div>
                    </div>
                </label>
            </div>
            <div>
                <label>
                    <div class="icon no-fv"></div>
                    {!! Form::radio('calc-type', '2', false) !!}
                    Analiza bez wykorzystania faktury
                    <div class="radio">
                        <div></div>
                    </div>
                </label>
            </div>
        </div>

        <div id="wrapper-product">
            <h4>Wybierz media, które Cię interesują:</h4>
            <div>
                <label>
                    <div class="icon g"></div>
                    {!! Form::radio('product', '2', false) !!}
                    Gaz
                    <div class="radio">
                        <div></div>
                    </div>
                </label>
            </div>
            <div>
                <label>
                    <div class="icon e"></div>
                    {!! Form::radio('product', '1', false) !!}
                    Prąd
                    <div class="radio">
                        <div></div>
                    </div>
                </label>
            </div>
            <div>
                <label>
                    <div class="icon eg"></div>
                    {!! Form::radio('product', '3', true) !!}
                    Gaz + Prąd
                    <div class="radio">
                        <div></div>
                    </div>
                </label>
            </div>
        </div>

        <div id="wrapper-vendor">
            <div id="wrapper-vendor-e" class="inputWrapper">
                <label for="vendor-e">Twój aktualny sprzedawca prądu<span><br>(potrzebne do obliczenia oszczędności na żarówkach LED’owych)</span></label>
                {!! Form::select(
                    'vendor-e',
                    array(
                        '0' => 'Wybierz dostawcę..',
                        'PGE' => 'PGE',
                        'TAURON' => 'TAURON',
                        'ENERGA' => 'ENERGA',
                        'ENEA' => 'ENEA',
                        'RWE' => 'RWE',
                        'INNY' => 'INNY'
                    ),
                    '0',
                    array('id' => 'vendor-e', 'class' => 'userInput required')
                ) !!}
            </div>
            <div id="wrapper-vendor-g" class="inputWrapper">
                <label for="vendor-g">Twój aktualny sprzedawca gazu</label>
                {!! Form::select(
                    'vendor-g',
                    array(
                        'PGNIG' => 'PGNIG',
                    ),
                    'PGNIG',
                    array('id' => 'vendor-g', 'class' => 'userInput')
                ) !!}
            </div>
            <div id="step2" class="start-btn"></div>
        </div>

        <div id="wrapper-g" class="group">
            <h4>Wpisz dane z faktury <span>gazowej</span></h4>

            <div class="invoice inputWrapper">
                <label for="tariff-g">Taryfa</label>
                {!! Form::select(
                    'tariff-g',
                    array(
                        '0' => 'Wybierz..',
                        '1' => 'W-1',
                        '2' => 'W-2',
                        '3' => 'W-3',
                        '4' => 'W-4'
                    ),
                    '0',
                    array('id' => 'tariff-g', 'class' => 'userInput')
                ) !!}
            </div>

            <div class="invoice inputWrapper">
                <label for="date-from-g">Faktura od</label>
                {!! Form::text('date-from-g', null, array('id' => 'date-from-g', 'class' => 'datepicker userInput', 'placeholder' => "DD-MM-RRRR"))  !!}
            </div>

            <div class="invoice inputWrapper">
                <label for="date-to-g">Faktura do</label>
                {!! Form::text('date-to-g', null, array('id' => 'date-to-g', 'class' => 'datepicker userInput', 'placeholder' => "DD-MM-RRRR"))  !!}
            </div>

            <div class="invoice inputWrapper">
                <label for="quantity-g">Zużycie gazu</label>
                {!! Form::text('quantity-g', null, array('id' => 'quantity-g', 'class' => 'userInput', 'placeholder' => "kWh"))  !!}
            </div>

            <div class="no-invoice hide inputWrapper">
                <label for="cost-g">Koszt miesięczny największej faktury</label>
                {!! Form::text('cost-g', null, array('id' => 'cost-g', 'class' => 'userInput', 'placeholder' => "zł"))  !!}
            </div>

            <div class="calc-output-wrapper inputWrapper">
                <label for="usage-g">Zużycie roczne</label>
                {!! Form::text('usage-g', "---- kWh", array('id' => 'usage-g', 'class' => 'usage', 'readonly'))  !!}
            </div>

            <div class="calc-savings-wrapper">
                <label for="savings-g">Oszczędzasz</label>
                <span class="d">W skali całego okresu umowy</span>
                <span class="t">W skali całej umowy</span>
                {!! Form::text('savings-g', "---,-- zł", array('id' => 'savings-g', 'class' => 'savings', 'readonly'))  !!}
            </div>
        </div>

        <div id="wrapper-e" class="group">
            <h4>Wpisz dane z faktury <span>za prąd</span></h4>

            <div class="invoice inputWrapper">
                <label for="tariff-e">Taryfa</label>
                {!! Form::select(
                    'tariff-e',
                    array(
                        '0' => 'Wybierz..',
                        '1' => 'G11',
                        '2' => 'G12',
                        '3' => 'G12W'
                    ),
                    '0',
                    array('id' => 'tariff-e', 'class' => 'userInput')
                ) !!}
            </div>

            <div class="invoice inputWrapper">
                <label for="date-from-e">Faktura od</label>
                {!! Form::text('date-from-e', null, array('id' => 'date-from-e', 'class' => 'datepicker userInput', 'placeholder' => "DD-MM-RRRR"))  !!}
            </div>

            <div class="invoice inputWrapper">
                <label for="date-to-e">Faktura do</label>
                {!! Form::text('date-to-e', null, array('id' => 'date-to-e', 'class' => 'datepicker userInput', 'placeholder' => "DD-MM-RRRR"))  !!}
            </div>

            <div class="invoice inputWrapper">
                <label for="quantity-e">Zużycie prądu</label>
                {!! Form::text('quantity-e', null, array('id' => 'quantity-e', 'class' => 'userInput', 'placeholder' => "kWh"))  !!}
            </div>

            <div class="no-invoice hide inputWrapper">
                <label for="cost-e">Koszt przeciętnej miesięcznej faktury</label>
                {!! Form::text('cost-e', null, array('id' => 'cost-e', 'class' => 'userInput', 'placeholder' => "zł"))  !!}
            </div>

            <div class="calc-output-wrapper inputWrapper">
                <label for="usage-e">Zużycie roczne</label>
                {!! Form::text('usage-e', "---- kWh", array('id' => 'usage-e', 'class' => 'usage', 'readonly'))  !!}
            </div>

            <div class="calc-savings-wrapper">
                <label for="savings-e">Oszczędzasz</label>
                <span class="d">W skali całego okresu umowy</span>
                <span class="t">W skali całej umowy</span>
                {!! Form::text('savings-e', "---,-- zł", array('id' => 'savings-e', 'class' => 'savings', 'readonly'))  !!}
            </div>

        </div>

        <div id="wrapper-b" class="group">
            <h4>Otrzymujesz żarówki <span>LED w prezencie</span></h4>

            <div class="inputWrapper">
                <label for="led">Liczba żarówek LED</label>
                {!! Form::text('led', null, array('id' =>'led', 'readonly'))  !!}
            </div>
            <div class="inputWrapper">
                <label for="usage-time-b" class="withspan">Średni czas użytkowania<br><span>Standardowo 240 minut</span></label>
                {!! Form::text('usage-time-b', null, array('id' => 'usage-time-b', 'class' => 'userInput required', 'placeholder' => "minut(-y)/dzień"))  !!}
            </div>
            <div class="calc-output-wrapper inputWrapper first">
                <label for="usage-std-b">Ilość energii zużytej przez standardowe żarówki</label>
                {!! Form::text('usage-std-b', "---,-- kWh/rok", array('id' => 'usage-std-b', 'class' => 'usage', 'readonly'))  !!}
            </div>
            <div class="calc-output-wrapper inputWrapper">
                <label for="usage-led-b">Ilość energii zużytej przez żarówki LED</label>
                {!! Form::text('usage-led-b', "---,-- kWh/rok", array('id' => 'usage-led-b', 'class' => 'usage', 'readonly'))  !!}
            </div>
            <div class="calc-output-wrapper inputWrapper last">
                <label for="savings-e-b">Oszczędność energii elektrycznej</label>
                {!! Form::text('savings-e-b', "---,-- kWh/rok", array('id' => 'savings-e-b', 'class' => 'savings', 'readonly'))  !!}
            </div>
            <div class="calc-savings-wrapper">
                <label for="savings-b">Oszczędzasz</label>
                {!! Form::text('savings-b', "---,-- zł", array('id' => 'savings-b', 'class' => 'savings', 'readonly'))  !!}
            </div>
            <div id="step3" class="start-btn"></div>
        </div>

        <div id="wrapper-summary" class="group">
            <div class="calc-savings-wrapper">
                <label for="savings-s"><span class="t m">Łącznie</span></label>
                <span class="no-d">Przechodząc do <span>Fortum</span> zaoszczędzisz łącznie</span>
                {!! Form::text('savings-s', "---,-- zł", array('id' => 'savings-s', 'class' => 'savings', 'readonly'))  !!}
            </div>
        </div>

        <h6>Szczegółowa analiza Twoich rachunków jest już gotowa</h6>

        <div id="wrapper-contact-data">
            {!! Form::text('name', null, array('id' => 'name', 'class' => 'userInput required', 'placeholder' => 'Imię*'))  !!}
            {!! Form::text('surname', null, array('id' => 'surname', 'class' => 'userInput required', 'placeholder' => 'Nazwisko*'))  !!}
            {!! Form::text('phone', null, array('id' => 'phone', 'class' => 'userInput required', 'placeholder' => 'Telefon*'))  !!}
            {!! Form::text('email', null, array('id' => 'email', 'class' => 'userInput required', 'placeholder' => 'Adres e-mail*'))  !!}
        </div>

        <div id="wrapper-agree">
            <div>
                <label>
                    {!! Form::checkbox('agree_1', 1, true, array('class' => 'userInput required') ) !!}
                    <div class="radio">
                        <div></div>
                    </div>
                    <div class="title">Zgoda na otrzymywanie informacji marketingowych oraz handlowych</div>
                </label>
                <div class="toggleBtn">
                    <div>Pokaż treść<span> zgody</span></div>
                    <div>Ukryj treść<span> zgody</span></div>
                </div>
                <p>
                    Oświadczam, że wyrażam zgodę na otrzymywanie informacji marketingowych oraz handlowych od Fortum Marketing and Trading S.A. oraz jej partnerów handlowych za
                    pomocą środków komunikacji
                    elektronicznej oraz środków bezpośredniego porozumiewania się na odległość, w szczególności na podany przeze mnie adres e-mail lub numer telefonu.
                </p>
            </div>
            <div>
                <label>
                    {!! Form::checkbox('agree_2', 1, true, array('class' => 'userInput required') ) !!}
                    <div class="radio">
                        <div></div>
                    </div>
                    <div class="title">Zgoda na przetwarzanie danych osobowych</div>
                </label>
                <div class="toggleBtn">
                    <div>Pokaż treść<span> zgody</span></div>
                    <div>Ukryj treść<span> zgody</span></div>
                </div>
                <p>
                    Oświadczam, że wyrażam zgodę na przetwarzanie moich danych osobowych przez Fortum Marketing and Trading Spółkę Akcyjną z/s w Gdańsku [dalej: Fortum] dla celów
                    promocji oraz marketingu
                    produktów i usług własnych Fortum oraz jego partnerów handlowych.
                </p>
            </div>
            <div>
                <label>
                    {!! Form::checkbox('agree_3', 1, true, array('class' => 'userInput required') ) !!}
                    <div class="radio">
                        <div></div>
                    </div>
                    <div class="title">Oświadczenie o otrzymaniu informacji</div>
                </label>
                <div class="toggleBtn">
                    <div>Pokaż treść<span> zgody</span></div>
                    <div>Ukryj treść<span> zgody</span></div>
                </div>
                <p>
                    Zostałem poinformowany, iż:<br>
                    a) administratorem moich danych osobowych jest Fortum Marketing and Trading Spółka Akcyjna z/s w Gdańsku, ul. Heweliusza 9,<br>
                    b) dane osobowe przetwarzane będą wyłącznie w celu promocji oraz marketingu produktów i usług własnych Fortum oraz partnerów handlowych Fortum, jak również, że moje
                    dane osobowe nie będą
                    udostępniane innym Odbiorcom,<br>
                    c) przysługuje mi prawo dostępu do treści moich danych osobowych oraz ich poprawiania, modyfikowania lub usunięcia,<br>
                    d) podanie Sprzedawcy danych osobowych jest dobrowolne.
                </p>
            </div>
            @if($agent)
                @include('partials.form_agent')
            @endif
            <div id="info-wrapper">
                <div id="error" class="info error">Wypełnij wszystkie wymagane pola</div>
                <div id="error-phone" class="info error">Nieprawidłowy format numeru telefonu</div>
                <div id="success-sending" class="info success">Twoja analiza została wysłana na podany adres e-mail</div>
                <div id="error-sending" class="info error">Wystąpił błąd podczas wysyłania analizy - spróbuj ponownie później</div>
                <div id="error-form" class="info error">Formularz zawiera błędy</div>
                <div class='uil-fortum-css'>
                    <div></div>
                    <div></div>
                </div>
            </div>

            {!! Form::submit('Pobierz analizę', array('id' => 'send-form'))  !!}
        </div>

        {!! Form::close() !!}
        <meta name="_token" content="{!! csrf_token() !!}"/>

        <footer>&copy; 2016 Fortum, Wszystkie prawa zastrzeżone.</footer>

        <div id="resp"></div>
    </div>

    <div id="response-modal-wrapper">
        <div class="modal">
            <img src="img/logo-fortum_c.png" />
            <h4 class="success">Dziękujemy za wypełnienie formularza.</h4>
            <p class="success">Twoja analiza została wysłana!</p>
            <h4 class="error">Wystąpił błąd podczas wysyłania analizy.</h4>
            <p class="error">Spróbuj ponownie później.</p>
            <button id="close-modal">Powrót</button>
        </div>
    </div>

    <script>
        function Constants() {

            @foreach($variables as $variable)
            /* {!! $variable->title !!} */
                {!! 'var ' . $variable->name . ' = ' . $variable->value . ';' !!}
            @endforeach

            var gasPrice = {
                        PGNIG: PGNIG_v,
                        DUON: DUON_v
                    };

            var power = {
                std: std_v,
                led: led_v
            };

            var product_name = [
                "Single Fuel EE",
                "Single Fuel GAZ",
                "Dual Fuel"
            ];

            this.getMonthsIndicator = function (i) {
                return Number(monthsIndicator[i]);
            };

            this.getSeason = function (date) {
                return seasons[date.month()];
            };

            this.getSeasonIndicator = function (season) {
                return seasons.reduce(function (a, e, i) {
                    if (e === season)
                        a += monthsIndicator[i];
                    return Number(a);
                }, []);
            };

            this.getGasPrice = function (vendor, tariff) {
                return Number(gasPrice[vendor][tariff]);
            };

            this.getGasSubscriptionPrice = function (tariff) {
                return Number(gasSubscriptionPrice[tariff]);
            };

            this.getGasNetworkConstant = function (tariff) {
                return Number(gasNetworkConstant[tariff]);
            };

            this.getGasNetworkVariable = function (tariff) {
                return Number(gasNetworkVariable[tariff]);
            };

            this.getEnergyPrice = function (vendor) {
                return Number(energyPrice[vendor]);
            };

            this.getEnergyNetworkConstant = function (vendor) {
                return Number(energyNetworkConstant[vendor]);
            };

            this.getOH = function (vendor) {
                return Number(OH[vendor]);
            }

            this.getEnergyNetworkVariable = function (vendor) {
                return Number(energyNetworkVariable[vendor]);
            }

            this.getBulbs = function (product) {
                return Number(bulbs[product_name[product - 1]]);
            }

            this.getPower = function (type, i) {
                return Number(power[type][i]);
            }

            this.getBulbPrice = function (vendor) {
                return Number(energyPrice[vendor]) + Number(energyNetworkVariable[vendor]);
            }
        }
    </script>
    <script src="{{ url('js/all.js') }}"></script>

@endsection
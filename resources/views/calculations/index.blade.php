@extends('layouts.master_admin')

@section('content')
    <h1>Zgłoszenia</h1>

    <table>
        <tbody>
        <tr>
            <th>Id</th>
            <th>Data wypełnienia</th>
            <th>Imię i nazwisko</th>
            <th>Telefon</th>
            <th>Email</th>
            <th>Taryfa EE</th>
            <th>Oszczędność EE</th>
            <th>Taryfa GAZ</th>
            <th>Oszczędność GAZ</th>
            <th>Oszczędność LED</th>
            <th>RAZEM</th>
            <th>Produkt</th>
            <th>Operator EE</th>
            <th>Wypełnione przez</th>
            <th>Uwagi</th>
        </tr>
        @foreach($calculations as $i => $calculation)
            <tr>
                <td>{{ $calculation->getAttribute('id') }}</td>
                <td>{{ $calculation->getAttribute('created_at') }}</td>
                <td>{{ $calculation->getAttribute('name') }} {{ $calculation->getAttribute('surname') }} </td>
                <td>{{ $calculation->getAttribute('phone') }}</td>
                <td class="email">{{ $calculation->getAttribute('email') }}</td>
                <td>{{ $calculation->getAttribute('tariff-e') }}</td>
                <td>{{ $calculation->getAttribute('savings-e') }}</td>
                <td>{{ $calculation->getAttribute('tariff-g') }}</td>
                <td>{{ $calculation->getAttribute('savings-g') }}</td>
                <td>{{ $calculation->getAttribute('savings-b') }}</td>
                <td>{{ $calculation->getAttribute('savings-s') }}</td>
                <td>{{ $calculation->getAttribute('product') }}</td>
                <td>{{ $calculation->getAttribute('vendor-e') }}</td>
                <td>{{ $calculation->getAttribute('agent') }}</td>
                <td>{{ $calculation->getAttribute('uwagi') }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div id="pagination">
        {{ $calculations->links() }}
    </div>

@endsection
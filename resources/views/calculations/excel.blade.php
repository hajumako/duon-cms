<!DOCTYPE html>
<html lang="pl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
        <tr>
            <th>Id</th>
            <th>Data wypełnienia</th>
            <th>Imię i nazwisko</th>
            <th>Telefon</th>
            <th>Email</th>
            <th>Taryfa EE</th>
            <th>Oszczędność EE</th>
            <th>Taryfa GAZ</th>
            <th>Oszczędność GAZ</th>
            <th>Oszczędność LED</th>
            <th>RAZEM</th>
            <th>Produkt</th>
            <th>Operator EE</th>
            <th>Wypełnione przez</th>
            <th>Uwagi</th>
        </tr>
        @foreach($calculations as $i => $calculation)
            <tr>
                <td>{{ $calculation['id'] }}</td>
                <td>{{ $calculation['created_at'] }}</td>
                <td>{{ $calculation['name'] }} {{ $calculation['surname'] }} </td>
                <td>{{ $calculation['phone'] }}</td>
                <td>{{ $calculation['email'] }}</td>
                <td>{{ $calculation['tariff-e'] }}</td>
                <td>{{ $calculation['savings-e'] }}</td>
                <td>{{ $calculation['tariff-g'] }}</td>
                <td>{{ $calculation['savings-g'] }}</td>
                <td>{{ $calculation['savings-b'] }}</td>
                <td>{{ $calculation['savings-s'] }}</td>
                <td>{{ $calculation['product'] }}</td>
                <td>{{ $calculation['vendor-e'] }}</td>
                <td>{{ $calculation['agent'] }}</td>
                <td>{{ $calculation['uwagi'] }}</td>
            </tr>
        @endforeach
</body>
</html>


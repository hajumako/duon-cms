<div class="inputWrapper agent">
    {!! Form::text('agent', null, array('id' => 'agent', 'class' => 'userInput required', 'placeholder' => 'Osoba wykonująca analizę*'))  !!}
</div>
<div class="inputWrapper uwagi">
    {!! Form::textarea('uwagi', null, array('id' => 'uwagi', 'class' => 'userInput', 'placeholder' => 'Uwagi', 'rows' => '10'))  !!}
</div>
<?php

use Illuminate\Database\Seeder;
use App\User as User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the user database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        User::create(['name' => 'Admin', 'email' => 'admin@admin.com', 'password' => bcrypt('123456')]);
        User::create(['name' => 'Konrad', 'email' => 'konrad.kucharski@quellio.com', 'password' => bcrypt('123456')]);
    }
}

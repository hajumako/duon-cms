<?php

use Illuminate\Database\Seeder;
use App\Variable as Variable;

class VariablesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('variables')->delete();
        Variable::create([
            'title' => 'Miesiąc',
            'name' => 'monthsIndicator',
            'cells_in_excel' => 'L2:L13',
            'value' => '[0.154929577,0.133802817,0.105633803,0.070422535,0.035211268,0.035211268,0.035211268,0.035211268,0.070422535,0.091549296,0.105633803,0.126760563]'
        ]);
        Variable::create([
            'title' => 'Miesiąc Pora roku',
            'name' => 'seasons',
            'cells_in_excel' => 'AG2:AG13',
            'value' => '["zima","zima","wiosna","wiosna","wiosna","lato","lato","lato","jesień","jesień","jesień","zima"]',
            'value_is_text' => true
        ]);
        Variable::create([
            'title' => 'GAZ PGNiG',
            'name' => 'PGNIG_v',
            'cells_in_excel' => 'C10:C11',
            'value' => '{"W1":0.0983,"W2": 0.0983,"W3": 0.0983,"W4": 0}'
        ]);
        Variable::create([
            'title' => 'GAZ DUON',
            'name' => 'DUON_v',
            'cells_in_excel' => 'C12:C13',
            'value' => '{"r2016":0.0885,"r2017":0.0885,"r2018":0.0983,"r2019":0.0983}'
        ]);
        Variable::create([
            'title' => 'Abonament',
            'name' => 'gasSubscriptionPrice',
            'cells_in_excel' => 'D10:D11',
            'value' => '{"W1":3.76,"W2":6.28,"W3":6.28,"W4":0}'
        ]);
        Variable::create([
            'title' => 'Stawka sieciowa stała',
            'name' => 'gasNetworkConstant',
            'cells_in_excel' => 'E10:E11',
            'value' => '{"W1":4.085,"W2":23.01,"W3":23.01,"W4":0}'
        ]);
        Variable::create([
            'title' => 'Stawka sieciowa zmienna',
            'name' => 'gasNetworkVariable',
            'cells_in_excel' => 'F10:F11',
            'value' => '{"W1":0.05413,"W2":0.03917,"W3":0.03917,"W4":0}'
        ]);
        Variable::create([
            'title' => 'EE czynna',
            'name' => 'energyPrice',
            'cells_in_excel' => 'C2:C7',
            'value' => '{"PGE":0.253,"TAURON":0.2546,"ENERGA":0.2521,"ENEA":0.2545,"RWE":0.2762,"INNY":0.25808,"DUON":0.2495}'
        ]);
        Variable::create([
            'title' => 'Sieciowa stała+Abonament+Przejściowa',
            'name' => 'energyNetworkConstant',
            'cells_in_excel' => 'F2:F7',
            'value' => '{"PGE":8.66,"TAURON":7.86,"ENERGA":9.02,"ENEA":10.06,"RWE":13.05,"INNY":9.73,"DUON":0}'
        ]);
        Variable::create([
            'title' => 'Sieciowa zmienna+jakościowa',
            'name' => 'energyNetworkVariable',
            'cells_in_excel' => 'E2:E7',
            'value' => '{"PGE":0.2162,"TAURON":0.1751,"ENERGA":0.2425,"ENEA":0.1750,"RWE":0.1515,"INNY":0.19206,"DUON":0}'
        ]);
        Variable::create([
            'title' => 'OH',
            'name' => 'OH',
            'cells_in_excel' => 'D2:D7',
            'value' => '{"PGE":0,"TAURON":0,"ENERGA":0,"ENEA":0,"RWE":4.29,"INNY":0,"DUON":0}'
        ]);
        Variable::create([
            'title' => 'Żarówki',
            'name' => 'bulbs',
            'cells_in_excel' => 'Z2:Z4',
            'value' => '{"Single Fuel GAZ":2,"Single Fuel EE":2,"Dual Fuel":4}'
        ]);
        Variable::create([
            'title' => 'Moc żarówek standardowych',
            'name' => 'std_v',
            'cells_in_excel' => 'Q2:Q3',
            'value' => '[45,60]'
        ]);
        Variable::create([
            'title' => 'Moc żarówek ledowych',
            'name' => 'led_v',
            'cells_in_excel' => 'R2:R3',
            'value' => '[6,10]'
        ]);
    }
}

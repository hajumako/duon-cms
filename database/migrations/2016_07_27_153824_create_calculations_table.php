<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalculationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calculations', function (Blueprint $table) {
            $table->increments('id');
            $table->text('calc-type');
            $table->text('product');
            $table->text('vendor-e');
            $table->text('vendor-g');
            $table->text('tariff-g');
            $table->text('date-from-g');
            $table->text('date-to-g');
            $table->text('quantity-g');
            $table->text('cost-g');
            $table->text('usage-g');
            $table->text('savings-g');
            $table->text('tariff-e');
            $table->text('date-from-e');
            $table->text('date-to-e');
            $table->text('quantity-e');
            $table->text('cost-e');
            $table->text('usage-e');
            $table->text('savings-e');
            $table->text('led');
            $table->text('usage-std-b');
            $table->text('usage-led-b');
            $table->text('savings-e-b');
            $table->text('savings-b');
            $table->text('savings-s');
            $table->text('name');
            $table->text('surname');
            $table->text('phone');
            $table->text('email');
            $table->text('agree_1');
            $table->text('agree_2');
            $table->text('agree_3');
            $table->text('ip');
            $table->text('agent');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('calculations');
    }
}

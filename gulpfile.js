var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix
        .sass('main.scss')
        .sass('media.scss')
        .sass('busy_anim.scss')
        .sass('admin.scss')
        .copy('resources/assets/js/jquery-2.1.1.min.js', 'public/js/jquery-2.1.1.min.js')
        .scripts([
            'moment.js',
            'pl.js',
            'bootstrap-datetimepicker.min.js'
        ], 'public/js/datepicker.js')
        .scripts([
            'main.js'
        ], 'public/js/all.js');
        //.version('js/all.js');
});

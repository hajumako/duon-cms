<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CalculationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    protected function getValidatorInstance()
    {
        $d = $this->all();
        $v = parent::getValidatorInstance();
        $v->after(function() use ($v, $d) {
            if ($this->isEmpty($d['calc-type']))
                $v->errors()->add('calc-type', 'Typ analizy jest wymagany.');
            if ($this->isEmpty($d['product']))
                $v->errors()->add('product', 'Należy wybrać media.');
            if (($d['product'] == 1 || $d['product'] == 3) && $this->isEmpty($d['vendor-e']))
                $v->errors()->add('vendor-e', 'Należy wybrać sprzedawcę prądu.');
            if (($d['product'] == 2 || $d['product'] == 3) && $this->isEmpty($d['vendor-g']))
                $v->errors()->add('vendor-g', 'Należy wybrać sprzedawcę gazu.');
            if ($d['calc-type'] == 1) {
                if ($d['product'] == 1 || $d['product'] == 3) {
                    if ($this->isEmpty($d['tariff-e']))
                        $v->errors()->add('tariff-e', 'Pole "Taryfa" jest wymagane.');
                    if ($this->isEmpty($d['date-from-e']))
                        $v->errors()->add('date-from-e', 'Pole "Faktura od" jest wymagane.');
                    if ($this->isEmpty($d['date-to-e']))
                        $v->errors()->add('date-to-e', 'Pole "Faktura do" jest wymagane.');
                    if ($this->isEmpty($d['quantity-e']))
                        $v->errors()->add('quantity-e', 'Pole "Zużycie gazu" jest wymagane.');
                }
                if ($d['product'] == 2 || $d['product'] == 3) {
                    if ($this->isEmpty($d['tariff-g']))
                        $v->errors()->add('tariff-g', 'Pole "Taryfa" jest wymagane.');
                    if ($this->isEmpty($d['date-from-g']))
                        $v->errors()->add('date-from-g', 'Pole "Faktura od" jest wymagane.');
                    if ($this->isEmpty($d['date-to-g']))
                        $v->errors()->add('date-to-g', 'Pole "Faktura do" jest wymagane.');
                    if ($this->isEmpty($d['quantity-g']))
                        $v->errors()->add('quantity-g', 'Pole "Zużycie prądu" jest wymagane.');
                }
            } elseif ($d['calc-type'] == 2) {
                if (($d['product'] == 1 || $d['product'] == 3) && $this->isEmpty($d['cost-e']))
                    $v->errors()->add('cost-e', 'Koszt przeciętnej miesięcznej faktury jest wymagany.');
                if (($d['product'] == 2 || $d['product'] == 3) && $this->isEmpty($d['cost-g']))
                    $v->errors()->add('cost-g', 'Koszt miesięczny największej faktury jest wymagany');
            }
            if ($this->isEmpty($d['led']))
                $v->errors()->add('led', 'Średni czas użytkowania jest wymagany.');
            if ($this->isEmpty($d['name']))
                $v->errors()->add('name', 'Imię jest wymagane.');
            if ($this->isEmpty($d['surname']))
                $v->errors()->add('surname', 'Nazwisko jest wymagane.');
            if ($this->isEmpty($d['phone']))
                $v->errors()->add('phone', 'Nr telefonu jest wymagany.');
            if ($this->isEmpty($d['email']))
                $v->errors()->add('email', 'Adres jest wymagany.');
            if (!isset($d['agree_1']))
                $v->errors()->add('agree_1', 'Zgoda na otrzymywanie informacji marketingowych oraz handlowych jest wymagana.');
            if (!isset($d['agree_2']))
                $v->errors()->add('agree_2', 'Zgoda na otrzymywanie informacji marketingowych oraz handlowych jest wymagana.');
            if (!isset($d['agree_3']))
                $v->errors()->add('agree_3', 'Zgoda na przetwarzanie danych osobowych jest wymagana.');
        });

        return $v;
    }
    
    protected function isEmpty($val)
    {
        return $val === "" || $val === 0 || $val === "0";
    }
}

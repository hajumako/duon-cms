<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::auth();

Route::get('/', 'CalculationsController@create_agent');
Route::get('/formularz', 'CalculationsController@create');
Route::get('/zgloszenia', 'CalculationsController@index');
//Route::get('/parametry', 'VariablesController@edit');
//Route::get('/parametry/zapisz', 'VariablesController@update');
Route::resource('/parametry', 'VariablesController');
Route::post('/calculate', 'CalculationsController@store');

Route::get('zgloszenia/generuj', 'CalculationsController@generate');
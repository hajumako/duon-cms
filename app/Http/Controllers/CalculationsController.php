<?php

namespace App\Http\Controllers;

use App\Variable;
use Mockery\CountValidator\Exception;
use Request;
use App\Calculation;
use App\Http\Requests;
use App\Http\Requests\CalculationRequest;
use Symfony\Component\HttpFoundation\Response;
use App\ClassExtensions\TCPDF_Ext;
use PHPMailer;
use Excel;

class CalculationsController extends Controller
{
    protected $product = array(
        "0" => "Brak",
        "1" => "Prąd",
        "2" => "Gaz",
        "3" => "Gaz + Prąd"
    );

    protected $tariff_e = array(
        "0" => "Brak",
        "1" => "G11",
        "2" => "G12",
        "3" => "G12W"
    );

    protected $tariff_g = array(
        "0" => "Brak",
        "1" => "W1",
        "2" => "W2",
        "3" => "W3",
        "4" => "W4",
    );

    public function __construct()
    {
        $this->middleware('auth', ['only' => ['index']]);
    }

    public function index()
    {
        $calculations = Calculation::orderBy('created_at', 'desc')->paginate(50);
        return view('calculations.index', ['calculations' => $calculations]);
    }

    public function create()
    {
        $variables = Variable::all();
        return view('calculations.create', ['variables' => $variables, 'agent' => false]);
    }

    public function create_agent()
    {
        $variables = Variable::all();
        return view('calculations.create', ['variables' => $variables, 'agent' => true]);
    }

    public function store(CalculationRequest $request)
    {
        $input = Request::all();

        $pdf = $this->generatePDF($input);
        if ($this->sendEmail($pdf)) {
            $input['ip'] = $this->get_client_ip();
            $input['product'] = $this->product[$input['product']];
            $input['tariff-e'] = $this->tariff_e[$input['tariff-e']];
            $input['tariff-g'] = $this->tariff_g[$input['tariff-g']];

            Calculation::create($input);

            return Response::HTTP_OK;
        } else
            return Response::HTTP_INTERNAL_SERVER_ERROR;
    }

    public function generate()
    {
        $filename = 'ListaZgłoszeń_' . date('Y-m-d_h:i:s');
        Excel::create($filename, function ($excel) {

            // Set the title
            $excel->setTitle('Analiza oszczęndości');

            // Chain the setters
            $excel->setCreator('Fortum')
                ->setCompany('Fortum');

            // Call them separately
            $excel->setDescription('Lista zgłoszeń');

            $excel->sheet('Zgłoszenia', function ($sheet) {
                $calculations = Calculation::all()->toArray();
                $sheet->loadView('calculations.excel')->with('calculations', $calculations);
            });

        })->download('xlsx');
    }

    private function get_client_ip()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if (isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    private function generatePDF($formData)
    {
        $pdf = new TCPDF_Ext(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Fortum');
        $pdf->SetTitle('Kalkulator oszczędności');
        $pdf->SetSubject('Kalkulator oszczędności energii elektrycznej');
        $pdf->SetKeywords('Fortum, oszczędności, kalkulator');
        $pdf->SetFont('freesans');
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);        // set some language-dependent strings (optional)
        $pdf->setFontSubsetting(true);
        $pdf->setPrintFooter(true);
        $pdf->SetMargins(20, 10, 20);
        $pdf->setFooterMargin(23);
        $pdf->setHeaderFont(array('freesans', '', ''));
        $pdf->setFooterFont(array('freesans', '', ''));

        $pdf->AddPage();

        $lh = "9";
        $html = '
<html lang="pl">

<head>
    <meta charset="UTF-8">
    <title>Kalkulator oszczędności - Fortum</title>
</head>

<body style="font-size:15px">
    <div style="width:100%;line-height:40px"></div>
    <table style="width:100%">
        <tbody>
            <tr>
                <td style="width:60%;line-height:' . $lh . 'px;">
                    <p style="margin:0">Kalkulacja dla</p>
                </td>
                <td style="width:39%;line-height:' . $lh . 'px;">
                    <p style="margin:0;color:#5ac37d;"><b>{{name}} {{surname}}</b></p>
                    <p style="margin:0">{{email}}</p>
                    <p style="margin:0">{{phone}}</p>
                </td>
            </tr>
        </tbody>
    </table>
    <div style="width:100%;border-bottom:2px solid #E3E3E3;line-height: 5px">
        <p></p>
    </div>    
    <div style="width:100%;line-height: 5px"></div>
    <table style="width:100%;">
        <tbody>
            <tr>
                <td style="width:60%;line-height:' . $lh . 'px;">
                    <p style="margin:0">Analiza z wykorzystaniem faktury</p>
                    <p style="margin:0">Wybrane media</p>
                </td>
                <td style="width:39%;line-height:' . $lh . 'px;">
                    <p style="margin:0">{{calc-type}}</p>
                    <p style="margin:0">{{product}}</p>
                </td>
            </tr>
            <tr>';
        if ($formData['product'] == "1")
            $html .= '
                <td style="width:60%;line-height:' . $lh . 'px;">
                    <p style="margin:0">Aktualny sprzedawca prądu</p>
                </td>
                <td style="width:39%;line-height:' . $lh . 'px;">
                    <p style="margin:0">{{vendor-e}}</p>
                </td>';
        elseif ($formData['product'] == "2")
            $html .= '
                <td style="width:60%;line-height:' . $lh . 'px;">
                    <p style="margin:0">Aktualny sprzedawca gazu</p>
                    <p style="margin:0">Aktualny sprzedawca prądu</p>
                </td>
                <td style="width:39%;line-height:' . $lh . 'px;">
                    <p style="margin:0">{{vendor-g}}</p>
                    <p style="margin:0">{{vendor-e}}</p>
                </td>';
        elseif ($formData['product'] == "3")
            $html .= '
                <td style="width:60%;line-height:' . $lh . 'px;">
                    <p style="margin:0">Aktualny sprzedawca prądu</p>
                    <p style="margin:0">Aktualny sprzedawca gazu</p>
                </td>
                <td style="width:39%;line-height:' . $lh . 'px;">
                    <p style="margin:0">{{vendor-e}}</p>
                    <p style="margin:0">{{vendor-g}}</p>
                </td>';
        $html .= '
            </tr>
        </tbody>
    </table>';

        if (isset($formData['product'])) {
            if ($formData['product'] == "2" || $formData['product'] == "3") {
                $html .= '   
            <div style="width:100%;line-height: 10px"></div>
            <table style="width:100%;border-bottom:2px solid #E3E3E3;">
                <tbody>
                    <tr>';
                if ($formData['calc-type'] == "1") {
                    $html .= '
                        <td style="width:60%;line-height:' . $lh . 'px;" valign="bottom">
                            <p style="margin:0;color:#78a0d4"><b>Dane z faktury gazowej</b></p>
                            <p style="margin:0">Taryfa</p>
                            <p style="margin:0">Faktura od</p>
                            <p style="margin:0">Faktura do</p>
                            <p style="margin:0">Zużycie</p>
                        </td>
                        <td style="width:39%;line-height:' . $lh . 'px;" valign="bottom">
                            <p style="margin:0;line-height:13px;"></p>
                            <p style="margin:0">{{tariff-g}}</p>
                            <p style="margin:0">{{date-from-g}}</p>
                            <p style="margin:0">{{date-to-g}}</p>
                            <p style="margin:0">{{quantity-g}} kWh</p>
                        </td>';
                } else if ($formData['calc-type'] == "2") {
                    $html .= '
                        <td style="width:60%;line-height:' . $lh . 'px;" valign="bottom">
                            <p style="margin:0;color:#78a0d4"><b>Dane gazowe</b></p>
                            <p style="margin:0">Koszt miesięczny największej faktury</p>
                        </td>
                        <td style="width:39%;line-height:' . $lh . 'px;" valign="bottom">
                            <p style="margin:0;line-height:15px;"></p>
                            <p style="margin:0">{{cost-g}}</p>
                        </td>';
                }
                $html .= '
                    </tr>
                    <tr>
                        <td style="width:60%;line-height:13px;">
                            <p style="margin:0;color:#377a6c">Zużycie roczne</p>
                        </td>
                        <td style="width:39%;line-height:13px;">
                            <p style="margin:0;color:#5ac37d">{{usage-g}}</p>
                        </td>
                    </tr>            
                    <tr>
                        <td style="line-height:10px;" colspan="2">
                            <p style="margin:0"></p>
                        </td>
                    </tr>
                </tbody>
            </table>';
            }
        }

        if (isset($formData['product'])) {
            if ($formData['product'] == "1" || $formData['product'] == "3") {
                $html .= '
            <div style="width:100%;line-height: 10px"></div>
            <table style="width:100%;border-bottom:2px solid #E3E3E3;">
                <tbody>
                    <tr>';
                if ($formData['calc-type'] == "1") {
                    $html .= '
                        <td style="width:60%;line-height:' . $lh . 'px;" valign="bottom">
                            <p style="margin:0;color:#5ac37d"><b>Dane z faktury energetycznej</b></p>
                            <p style="margin:0">Taryfa</p>
                            <p style="margin:0">Faktura od</p>
                            <p style="margin:0">Faktura do</p>
                            <p style="margin:0">Zużycie</p>
                        </td>
                        <td style="width:39%;line-height:' . $lh . 'px;" valign="bottom">
                            <p style="margin:0;line-height:13px;"></p>
                            <p style="margin:0">{{tariff-e}}</p>
                            <p style="margin:0">{{date-from-e}}</p>
                            <p style="margin:0">{{date-to-e}}</p>
                            <p style="margin:0">{{quantity-e}} kWh</p>
                        </td>';
                } else if ($formData['calc-type'] == "2") {
                    $html .= '
                        <td style="width:60%;line-height:' . $lh . 'px;" valign="bottom">
                            <p style="margin:0;color:#5ac37d"><b>Dane energetyczne</b></p>
                            <p style="margin:0">Koszt przeciętnej miesięcznej faktury</p>
                        </td>
                        <td style="width:39%;line-height:' . $lh . 'px;" valign="bottom">
                            <p style="margin:0;line-height:15px;"></p>
                            <p style="margin:0">{{cost-e}}</p>
                        </td>';
                }
                $html .= '
                    </tr>
                    <tr>
                        <td style="width:60%;line-height:13px;">
                            <p style="margin:0;color:#377a6c">Zużycie roczne</p>
                        </td>
                        <td style="width:39%;line-height:13px;">
                            <p style="margin:0;color:#5ac37d">{{usage-e}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height:10px;" colspan="2">
                            <p style="margin:0"></p>
                        </td>
                    </tr>
                </tbody>
            </table>';
            }
        }

        $html .= '      
    <div style="width:100%;line-height: 10px"></div>
    <table style="width:100%;border-bottom:2px solid #E3E3E3;">
        <tbody>
            <tr>
                <td style="width:60%;line-height:' . $lh . 'px;" valign="bottom">
                    <p style="margin:0;color:#78dec8"><b>Żarówki LED w prezencie</b></p>
                    <p style="margin:0">Liczba żarówek LED</p>
                    <p style="margin:0">Średni czas użytkowania</p>
                </td>
                <td style="width:39%;line-height:' . $lh . 'px;" valign="bottom">
                    <p style="margin:0;line-height:15px;"></p>
                    <p style="margin:0">{{led}}</p>
                    <p style="margin:0">{{usage-time-b}} minut(-y)/dzień</p>
                </td>
            </tr>
            <tr>
                <td style="width:60%;line-height:' . $lh . 'px;">
                    <p style="margin:0;color:#377a6c">Ilość energii zużytej przez standardowe żarówki</p>
                    <p style="margin:0;color:#377a6c">Ilość energii zużytej przez żarówki LED</p>
                    <p style="margin:0;color:#377a6c">Oszczędność energii elektrycznej</p>
                </td>
                <td style="width:39%;line-height:' . $lh . 'px;">
                    <p style="margin:0;color:#5ac37d">{{usage-std-b}}</p>
                    <p style="margin:0;color:#5ac37d">{{usage-led-b}}</p>
                    <p style="margin:0;color:#5ac37d">{{savings-e-b}}</p>
                </td>
            </tr>            
            <tr>
                <td style="line-height:10px;" colspan="2">
                    <p style="margin:0"></p>
                </td>
            </tr>
        </tbody>
    </table>
    <div style="width:100%;line-height: 20px"></div>
    <table style="width:100%;border-top:2px solid #f8f8f8;">
        <tbody>
            <tr style="background-color:#f8f8f8;">
                <td style="width:60%;line-height:8px;" valign="center">
                    <p style="margin:0;color:#a9a9a9;"><b><span style="color:#f8f8f8">__</span>Przechodząc do <span style="color:#5ac37d">Fortum</span> zaoszczędzisz łącznie</b></p>
                </td>
                <td style="width:39%;line-height:8px;" valign="center">
                    <p style="margin:0;color:#5ac37d;"><b>{{savings-s}}</b></p>
                </td>
            </tr>
            <tr style="background-color:#f8f8f8;">
                <td style="width:99%;line-height:10px;" valign="center">
                </td>
            </tr>
            <tr style="background-color:#f8f8f8;">
                <td style="width:99%;line-height:10px;" valign="center">
                </td>
            </tr>
        </tbody>
    </table>';

        $html .= '
</body>
</html>';

        $datas = array(
            'name',
            'surname',
            'phone',
            'email',
            'vendor-e',
            'vendor-g',
            'date-from-e',
            'date-to-e',
            'quantity-e',
            'cost-e',
            'usage-e',
            'savings-e',
            'date-from-g',
            'date-to-g',
            'quantity-g',
            'cost-g',
            'usage-g',
            'savings-g',
            'led',
            'usage-time-b',
            'usage-std-b',
            'usage-led-b',
            'savings-e-b',
            'savings-b',
            'savings-s'
        );

        foreach ($datas as $data) {
            if (isset($formData[$data]))
                $html = str_replace("{{" . $data . "}}", $formData[$data], $html);
        }

        if (isset($formData['calc-type']))
            $html = str_replace("{{calc-type}}", ($formData['calc-type'] == '1' ? "TAK" : "NIE"), $html);

        if (isset($formData['product'])) {
            $html = str_replace("{{product}}", $this->product[$formData['product']], $html);
        }

        if (isset($formData['tariff-e']) && $formData['tariff-e'] !== "0")
            $html = str_replace("{{tariff-e}}", $this->tariff_e[$formData['tariff-e']], $html);

        if (isset($formData['tariff-g']) && $formData['tariff-g'] !== "0")
            $html = str_replace("{{tariff-g}}", $this->tariff_g[$formData['tariff-g']], $html);

        $pdf->writeHTML($html, true, 0, true, 0);
        $pdf->lastPage();

        return $pdf->Output('Analiza_oszczednosci_Fortum.pdf', 'S');
    }

    private function sendEmail($pdf)
    {
        $mail = new PHPMailer(true);
        try {
            $mail->isSMTP();
            $mail->SMTPDebug = 1;
            $mail->SMTPAuth = true;
            $mail->Host = config('phpmailer.MAIL_HOST');
            $mail->SMTPSecure = config('phpmailer.MAIL_ENCRYPTION');
            $mail->Port = config('phpmailer.MAIL_PORT');
            $mail->Username = config('phpmailer.MAIL_USERNAME');
            $mail->Password = config('phpmailer.MAIL_PASSWORD');

            $mail->CharSet = 'UTF-8';
            $mail->addReplyTo(config('phpmailer.MAIL_REPLY'), 'Fortum');
            $mail->setFrom(config('phpmailer.MAIL_FROM'), 'Fortum');
            $mail->addAddress($_POST['email'], $_POST['name'] . ' ' . $_POST['surname']);
            $mail->addBCC(config('phpmailer.MAIL_FROM'), 'Fortum');
            $mail->Subject = 'Fortum analiza oszczędności';

            $mailBodyClient = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Fortum - kalkulator oszczędności</title>
    <style type="text/css">
        #outlook a {padding:0;}
        .ExternalClass {width:100%;}
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
    </style>
</head>
<body style="width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;">
<table style="margin:0; padding:0; width:600px !important; line-height: 100% !important; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; background: white;">
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" border="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                <tr>
                    <td style="border-collapse: collapse;" width="16"  valign="top"></td>
                    <td style="border-collapse: collapse; padding-top: 22px; padding-bottom: 22px;" width="568"  valign="top">
                        <img style="outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; display:block;" src="http://analizaoszczednosci.fortum.com.pl/logo-fortum.png" alt="fortum">
                    </td>
                    <td style="border-collapse: collapse;" width="16"  valign="top"></td>
                </tr>
                <tr>
                    <td style="border-collapse: collapse; font-size: 3px; line-height: 3px;" width="16" height="3" valign="top">&nbsp;</td>
                    <td style="border-collapse: collapse; font-size: 3px; line-height: 3px; background: #e1e1e1;" width="568" height="3" valign="top">&nbsp;</td>
                    <td style="border-collapse: collapse; font-size: 3px; line-height: 3px;" width="16" height="3" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td style="border-collapse: collapse; " width="16" valign="top"></td>
                    <td style="border-collapse: collapse; padding-top: 25px; padding-bottom: 45px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify; line-height: 1.5;" width="568" valign="top">
                        Szanowny Kliencie!<br>
                        <br>
                        W załączeniu wysyłamy szczegółową analizę rachunków. Dziękujemy za skorzystanie z naszych usług oraz zapraszamy po więcej informacji na stronę internetową <a href="http://www.fortum.com.pl" target="_blank">www.fortum.com.pl</a><br>
                        <br>
                        Z poważaniem<br>
                        Fortum Marketing and Trading S.A.<br>
                        <br>
                        Fortum Marketing and Trading S.A.<br>
                        Sąd Rejonowy Gdańsk – Północ w Gdańsku, VII Wydział Gospodarczy Krajowego Rejestru Sądowego<br>
                        KRS 0000378299 NIP 7811861610 REGON 301677244<br>
                        Wysokość kapitału zakładowego: 15 000 000 PLN – wpłacony w całości<br>
                        <br>
                        Informacje dotyczące Fortum Marketing and Trading S.A. i jej działalności zawarte w niniejszym przekazie elektronicznym stanowią tajemnicę przedsiębiorstwa i przeznaczone są wyłącznie dla adresata. Jeżeli nie jesteś adresatem powiadom o tym fakcie nadawcę i zniszcz ten przekaz, wraz ze wszystkimi załącznikami, które zawiera. Posługiwanie się, ujawnianie, kopiowanie, zarówno w całości jak i w części, informacji tu zawartych jest zabronione i stanowi naruszenie prawa. Ponieważ nie możemy zapewnić, że niniejszy przekaz elektroniczny wolny jest od wirusów komputerowych, prosimy o podjęcie właściwych kroków celem sprawdzenia tego przekazu pod kątem ich obecności. Zastrzegamy sobie prawo wglądu do treści każdego przekazu elektronicznego, który wpływa do naszych systemów elektronicznych lub jest z nich wysyłany.
                    </td>
                </tr>
                <tr>
                    <td style="border-collapse: collapse; font-size: 3px; line-height: 3px;" width="16" height="3" valign="top">&nbsp;</td>
                    <td style="border-collapse: collapse; font-size: 3px; line-height: 3px; background: #e1e1e1;" width="568" height="3" valign="top">&nbsp;</td>
                    <td style="border-collapse: collapse; font-size: 3px; line-height: 3px;" width="16" height="3" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td style="border-collapse: collapse; " width="16" valign="top"></td>
                    <td style="border-collapse: collapse; padding-top: 25px; padding-bottom: 45px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify; line-height: 1.5;" width="568" valign="top">                    
                        The information related to the official business of Fortum Marketing and Trading S.A. contained in this electronic communication and any accompanying documents is confidential and is intended only for the use of the addressee. If you have received this communication in error, please notify the sender immediately by return e-mail, and destroy this communication, including all attachments. Unauthorized use, disclosure or copying of this communication, or any part thereof, is strictly prohibited and unlawful. We cannot assure that this electronic communication or its attachments are free from viruses. In keeping with good computing practice, please take adequate steps to check for any viruses. We reserve the right to access and read all electronic communications and attachments entering or leaving our electronic systems.<br>
                    </td>
                    <td style="border-collapse: collapse;" width="3" valign="top"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
        ';

            $mail->msgHTML($mailBodyClient);

            if ($pdf) {
                $mail->addStringAttachment($pdf, "Fortum kalkulator oszczędności.pdf");
            }

            if ($mail->send()) {
                return true;
            } else {
                return false;
            }
        } catch (phpmailerException $e) {

        } catch (Exception $e) {

        }
    }
}

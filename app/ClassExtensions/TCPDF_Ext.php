<?php

namespace App\ClassExtensions;

use TCPDF;

class TCPDF_Ext extends TCPDF
{
    public function Header() {
        if (count($this->pages) === 1) { // Do this only on the first page
            $html = '
                <table style="width:100%;margin:0;padding:0;border:none">
                    <tbody style="margin:0;padding:0;border:none">
                        <tr>
                            <td style="height: 20px;line-height: 20px">
                            </td>
                        </tr>
                        <tr style="margin:0;padding:0;border:none">
                            <td style="margin:0;padding:0;border:none;width:60%;line-height:20px;" valign="bottom">
                                <img width="100" src="logo-fortum.png"/>
                            </td>
                            <td style="margin:0;padding:0;border:none;width:39%;line-height:13px;text-align: right" valign="bottom">
                                <p style="margin:0;font-size: 13px;vertical-align: text-bottom">Data utworzenia: <span style="font-size:14px;vertical-align: text-bottom"><b>' . date("d.m.Y") . '</b></span></p>
                            </td>
                        </tr>
                          <tr>
                            <td style="height: 7px;line-height: 7px">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div style="width:100%;border-bottom:2px solid #E3E3E3;margin:0;line-height: 5px"></div>';
            
            $this->writeHTML($html, true, false, false, false, '');
        }
    }

    public function Footer() {
        if (count($this->pages) === 1) { // Do this only on the first page
            $html = '
                <p style="text-align: center; font-size:13px; border-top:2px solid #E3E3E3;line-height: 30px;padding-top: 20px;margin: 0;color:#377a6c">
                    Jeśli jeszcze nie skorzystałeś z oferty, skontaktuj się z nami i oszczędzaj
                    <a style="color:#5ac37d;text-decoration: none" href="mailto:sprzedaz@fortum.com.pl">sprzedaz@fortum.com.pl</a>
                </p>
                <p style="text-align: center; font-size:12px;line-height: 5px;margin:0;color:#377a6c">
                    Dokument wygenerowany na stronie 
                    <b><a style="text-decoration: none;color:#377a6c" href="http://analizaoszczednosci.fortum.com.pl">analizaoszczednosci.fortum.com.pl</a></b>
                </p>';

            $this->writeHTML($html, true, false, false, false, '');
        }
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Calculation
 *
 * @property integer $id
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property string $tariff_e
 * @property string $savings_e
 * @property string $tariff_g
 * @property string $savings_g
 * @property string $savings_b
 * @property string $savings_total
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Calculation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Calculation whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Calculation whereSurname($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Calculation whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Calculation whereTariffE($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Calculation whereSavingsE($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Calculation whereTariffG($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Calculation whereSavingsG($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Calculation whereSavingsB($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Calculation whereSavingsTotal($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Calculation whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Calculation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Calculation extends Model
{
    protected $fillable = [
        'calc-type',
        'product',
        'vendor-e',
        'vendor-g',
        'tariff-g',
        'date-from-g',
        'date-to-g',
        'quantity-g',
        'cost-g',
        'usage-g',
        'savings-g',
        'tariff-e',
        'date-from-e',
        'date-to-e',
        'quantity-e',
        'cost-e',
        'usage-e',
        'savings-e',
        'led',
        'usage-std-b',
        'usage-led-b',
        'savings-e-b',
        'savings-b',
        'savings-s',
        'name',
        'surname',
        'phone',
        'email',
        'agree_1',
        'agree_2',
        'agree_3',
        'ip',
        'agent',
        'uwagi'
    ];
}
